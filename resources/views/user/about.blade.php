@extends('layouts.user.master')

@section('content')

<main class="bg-light">

    <div class="page-hero-section bg-image hero-mini" style="background-image: url(theme/assets/img/hero_mini.svg);">
        <div class="hero-caption">
            <div class="container fg-white h-100">
                <div class="row justify-content-center align-items-center text-center h-100">
                    <div class="col-lg-6">
                        <h3 class="mb-4 fw-medium text-warning">About Us</h3>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-dark justify-content-center bg-transparent">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">About</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-section pt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="card-page">
                        <h5 class="fg-warning">About Us</h5>
                        <hr>
                        <p>Hello, we are Aksantara Digital. Our story began in 2019 where founded by
                            some
                            university student Diponegoro. Our name
                            and logo is inspired by Javanese Culture, namely Wayang. We are moving in Information
                            Technology
                            especially development
                            and manufacture Website, Mobile App, Design, etc. Our vision is helping and realizing idea
                            our
                            client into reality,
                            while our mission is make a products that can help and make it easier for people in their
                            daily
                            lives.</p>
                    </div>
                    {{-- <div class="card-page mt-3">
                        <h5 class="fg-warning">Our Teams</h5>
                        <hr>

                        <div class="row justify-content-center">
                            <div class="col-lg-3 py-3">
                                <div class="team-item">
                                    <div class="team-avatar">
                                        <img src="theme/assets/img/person/person_1.png" alt="">
                                    </div>
                                    <h5 class="team-name">Sugar Elliot</h5>
                                    <span class="fg-gray fs-small">Creative Director</span>
                                </div>
                            </div>
                            <div class="col-lg-3 py-3">
                                <div class="team-item">
                                    <div class="team-avatar">
                                        <img src="theme/assets/img/person/person_2.png" alt="">
                                    </div>
                                    <h5 class="team-name">John Doe</h5>
                                    <span class="fg-gray fs-small">UI/UX Designer</span>
                                </div>
                            </div>
                            <div class="col-lg-3 py-3">
                                <div class="team-item">
                                    <div class="team-avatar">
                                        <img src="theme/assets/img/person/person_3.png" alt="">
                                    </div>
                                    <h5 class="team-name">Ellysa</h5>
                                    <span class="fg-gray fs-small">Product Manager</span>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>

</main> <!-- .bg-light -->


@endsection
