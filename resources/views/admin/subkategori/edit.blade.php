@extends('layouts.admin.app')

@section('content')

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Subkategori</h1>
    </div>



    <!-- Content Row -->

    <div class="row">


        <div class="col-xl-8 col-lg-7">
            <div class="card shadow mb-4">
                <form method="POST" enctype="multipart/form-data" action="/sub/{{ $subkategori->id }}/edit" class="m-3">
                    @csrf
                    @method('patch')
                    <div class="form-group">
                        <label>Nama</label>
                        <input value="{{ $subkategori->nama }}" name="nama" class="form-control" type="text"
                            placeholder="Nama">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>

    </div>


</div>


@endsection
