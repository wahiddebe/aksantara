<?php

namespace App\Http\Controllers;

use App\Android;
use App\Contact;
use App\Design;
use App\Setting;
use App\Subkategori;
use App\Supporting;
use App\Web;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {
        return view('welcome', [
            'settings' => Setting::find(1),
            'supporting' => Supporting::latest()->get(),
        ]);
    }

    public function about()
    {
        return view('user.about', [
            'settings' => Setting::find(1),
        ]);
    }

    public function portofolio()
    {
        return view('user.portofolio', [
            'settings' => Setting::find(1),
        ]);
    }

    public function support()
    {
        return view('user.support', [
            'settings' => Setting::find(1),
            'supporting' => Supporting::latest()->get(),
        ]);
    }
    public function contact()
    {
        return view('user.contact', [
            'settings' => Setting::find(1),
        ]);
    }
    public function webportofolio()
    {
        return view('user.subportofolio', [
            'settings' => Setting::find(1),
            'subkategoris' => Subkategori::get(),
            'items' => Web::where('kategori_id', '=', '1')->get(),
            'url' => 'web',
            'title' => 'Website'
        ]);
    }
    public function androidportofolio()
    {
        return view('user.subportofolio', [
            'settings' => Setting::find(1),
            'subkategoris' => Subkategori::get(),
            'items' => Android::where('kategori_id', '=', '2')->get(),
            'url' => 'android',
            'title' => 'Android'
        ]);
    }
    public function designportofolio()
    {
        return view('user.subportofolio', [
            'settings' => Setting::find(1),
            'subkategoris' => Subkategori::get(),
            'items' => Design::where('kategori_id', '=', '3')->get(),
            'url' => 'design',
            'title' => 'Design'
        ]);
    }
    public function webdetail(Web $web)
    {
        return view('user.detail', [
            'settings' => Setting::find(1),
            'subkategoris' => Subkategori::get(),
            'items' => Web::find($web->id),
            'url' => 'web',
            'title' => 'Website',
        ]);
    }
    public function androiddetail(Android $android)
    {
        return view('user.detail', [
            'settings' => Setting::find(1),
            'subkategoris' => Subkategori::get(),
            'items' => Android::find($android->id),
            'url' => 'android',
            'title' => 'Android',
        ]);
    }
    public function designdetail(Design $design)
    {
        return view('user.detail', [
            'settings' => Setting::find(1),
            'subkategoris' => Subkategori::get(),
            'items' => Design::find($design->id),
            'url' => 'design',
            'title' => 'Design',
        ]);
    }

    public function filter(Subkategori $subkategori)
    {
        return view('user.sort', [
            'settings' => Setting::find(1),
            'subs' => Subkategori::find($subkategori->id),
            'subkategoris' => Subkategori::get(),
            'webs' => Web::where('subkategori_id', '=', $subkategori->id)->get(),
            'androids' => Android::where('subkategori_id', '=', $subkategori->id)->get(),
            'designs' => Design::where('subkategori_id', '=', $subkategori->id)->get(),
        ]);
    }
}
