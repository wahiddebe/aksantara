<?php

namespace App\Http\Controllers;


use App\Kategori;
use App\Subkategori;
use App\Web;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class WebController extends Controller
{
    public function create()
    {
        return view("admin.web.create", [
            'selected' => 1,
            'kategori' => Kategori::get(),
            'subkategori'  => Subkategori::get(),
        ]);
    }
    public function edit(Web $web)
    {
        return view("admin.web.edit", [
            'web' => $web,
            'kategori' => Kategori::get(),
            'subkategori'  => Subkategori::get(),
        ]);
    }
    public function store(Request $request)
    {


        $request->validate([
            'foto_isi' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            'foto_judul' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
        ]);
        if ($request->hasFile('foto_judul')) {
            $foto_judul = $request->judul . '.' . $request->foto_judul->extension();
            $request->foto_judul->move(public_path('images/web/foto_judul'), $foto_judul);
        }
        if ($request->file('foto_judul') == null) {
            $foto_judul = null;
        }
        if ($request->hasFile('foto_isi')) {
            $foto_isi = $request->judul . '.' . $request->foto_isi->extension();
            $request->foto_isi->move(public_path('images/web/foto_isi'), $foto_isi);
        }
        if ($request->file('foto_isi') == null) {
            $foto_isi = null;
        }



        $web = new Web();
        $web->judul = $request->judul;
        $web->kategori_id = $request->kategori;
        $web->subkategori_id = $request->subkategori;
        $web->foto_judul = $foto_judul;
        $web->foto_isi = $foto_isi;
        $web->save();
        return redirect()->to('web');
    }
    public function update(Request $request, Web $web)
    {
        $request->validate([
            'foto_isi' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            'foto_judul' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
        ]);
        if ($request->hasFile('foto_judul')) {
            $foto_judul = $request->judul . '.' . $request->foto_judul->extension();
            $request->foto_judul->move(public_path('images/web/foto_judul'), $foto_judul);
        }
        if ($request->file('foto_judul') == null) {
            $foto_judul = $web->foto_judul;
        }
        if ($request->hasFile('foto_isi')) {
            $foto_isi = $request->judul . '.' . $request->foto_isi->extension();
            $request->foto_isi->move(public_path('images/web/foto_isi'), $foto_isi);
        }
        if ($request->file('foto_isi') == null) {
            $foto_isi = $web->foto_isi;
        }


        $web->judul = $request->judul;
        $web->kategori_id = $request->kategori;
        $web->subkategori_id = $request->subkategori;
        $web->foto_judul = $foto_judul;
        $web->foto_isi = $foto_isi;
        $web->update();
        return redirect()->to('web');
    }
    public function destroy(Web $web)
    {
        Storage::disk('aset')->delete('/images/web/foto_judul/' . $web->foto_judul);
        Storage::disk('aset')->delete('/images/web/foto_isi/' . $web->foto_isi);
        $web->delete();
        return redirect('web');
    }
}
