<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subkategori extends Model
{
    public function webs()
    {
        return $this->hasMany(Web::class);
    }
    public function androids()
    {
        return $this->hasMany(Web::class);
    }
    public function designs()
    {
        return $this->hasMany(Web::class);
    }
}
