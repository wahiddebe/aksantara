@extends('layouts.admin.app')

@section('content')

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Supporting Professional</h1>
        <a href="{{ route('create_supporting') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-upload fa-sm text-white-50"></i> Create New</a>
    </div>

    <!-- Content Row -->

    <div class="row">


        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4 p-2">
                <table width="100%" class=" table table-striped table-bordered table-hover" id="table">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Gambar</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $i=1
                        @endphp
                        @foreach ($supportings as $item)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>
                                <img class="w-25 mx-auto d-block " src="{{ asset('images/supporting/'.$item->gambar) }}"
                                    alt="">
                            </td>
                            <td>

                                <div class="button-group">
                                    <a href="/supporting/{{ $item->id }}/edit"><button
                                            class="btn-success btn btn-sm">Edit</button></a>
                                    <button class="btn-danger btn btn-sm" data-toggle="modal"
                                        data-target="#delete-item-{{ $item->id }}">Delete</button>
                                    <div class="modal fade" id="delete-item-{{ $item->id }}" tabindex="-1"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin
                                                        Menghapus {{ $item->nama }} ?</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div>
                                                        <div class="mb-2"></div>
                                                        <div class="text-secondary">
                                                            <small>
                                                                Published:{{$item->created_at->format('d M,y')}}
                                                            </small>
                                                        </div>
                                                    </div>
                                                    <form action="/supporting/{{ $item->id }}/delete" method="post">
                                                        @csrf
                                                        @method('delete')

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-success">Yes</button>
                                                            <button type="button" class="btn btn-danger"
                                                                data-dismiss="modal">No</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin Menghapusnya ?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="mb-2"></div>
                        <div class="text-secondary">
                            <small>
                                Published:
                            </small>
                        </div>
                    </div>
                    <form action="" method="post">
                        @csrf
                        @method('delete')

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Yes</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
