@extends('layouts.user.master')

@section('content')

<div class="page-hero-section bg-image hero-home-2" style="background-image: url(theme/assets/img/bg_hero_2.svg);">
    <div class="hero-caption">
        <div class="container fg-white h-100">
            <div class="row align-items-center h-100">
                <div class="col-lg-6 wow fadeInUp">
                    <div class="badge badge-soft mb-2">Welcome To</div>
                    <h1 class="mb-4 fw-bold">AKSANTARA DIGITAL</h1>
                    <p class="mb-4">Web Develloper,
                        Mobile application developer, &
                        also Design</p>

                    <a href="/contact" class="btn btn-warning">Hire Me</a>

                </div>
                <div class="col-lg-6 d-none d-lg-block wow zoomIn">
                    <div class="img-place mobile-preview shadow floating-animate mt-0">
                        <img src="theme/assets/favicon-light.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-section no-scroll">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12 pl-lg-5 wow fadeInUp">
                <h2 class="mb-4 text-center text-warning">About Aksantara</h2>
                <p class="mb-4 text-justify">Hello, we are Aksantara Digital. Our story began in 2019 where founded by
                    some
                    university student Diponegoro. Our name
                    and logo is inspired by Javanese Culture, namely Wayang. We are moving in Information Technology
                    especially development
                    and manufacture Website, Mobile App, Design, etc. Our vision is helping and realizing idea our
                    client into reality,
                    while our mission is make a products that can help and make it easier for people in their daily
                    lives.</p>
            </div>
        </div>
    </div>
</div>

<div class="page-section">
    <div class="container">
        <h2 class="text-center wow fadeIn mb-4 text-warning">Main Service</h2>
        <div class="row justify-content-center">
            <div class="col-lg-12 py-3 pl-lg-5">
                <div class="iconic-list">
                    <div class="iconic-item wow fadeInUp">
                        <div class="iconic-content">
                            <h5>Web Development</h5>
                            <p class="fs-small text-justify pr-5">Do you want to create a Company Profile, Inventory,
                                Cashier, Information
                                System, e-commerce, or your other ideas? Our
                                team is ready to discuss with you to make your idea come true.</p>
                        </div>
                        <div class="iconic-lg iconic-text  fg-white rounded-circle">
                            <span class=""><img class="img-fluid" src="theme/assets/img/icons/Website.png"
                                    alt=""></span>
                        </div>
                    </div>
                    <div class="iconic-item wow fadeInUp">
                        <div class="iconic-content">
                            <h5>Mobile Development</h5>
                            <p class="fs-small text-justify pr-5">Do you want to create a mobile application ? Our team
                                is
                                ready to help
                                you in create and develop application such as
                                simple applications, company profiles, e-commerce, marketplace, inventory, POS and other
                                ideas.</p>
                        </div>
                        <div class="iconic-lg iconic-text  fg-white rounded-circle">
                            <span class=""><img class="img-fluid" src="theme/assets/img/icons/Android.png"
                                    alt=""></span>
                        </div>
                    </div>
                    <div class="iconic-item wow fadeInUp">
                        <div class="iconic-content">
                            <h5>Design</h5>
                            <p class="fs-small text-justify pr-5">Our team is ready to help you to create design such as
                                logo, handbooks,
                                UI / UX mobile, UI/UX website, branding company,
                                creative proposals, creative CV, brochures, posters, banners, etc.</p>
                        </div>
                        <div class="iconic-lg iconic-text  fg-white rounded-circle">
                            <span class=""><img class="img-fluid" src="theme/assets/img/icons/Desain.png" alt=""></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-section no-scroll">
    <div class="container">
        <h2 class="text-center wow fadeIn mb-4 text-warning">Portofolio</h2>

        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="row justify-content-center mb-4 ">
                    <div class="col-md-6 col-lg-4 py-0 wow d-flex align-items-stretch fadeInLeft">
                        <div class="card card-body border-0 text-center shadow flex-fill pt-0">
                            <div class="svg-icon mx-auto mb-2">
                                <img class="img-fluid" src="theme/assets/img/icons/Website.png" alt="">
                            </div>
                            <h5 class="fg-gray">Web Develpment</h5>
                            <p class="fs-small text-justify">Do you want to create a Company Profile, Inventory,
                                Cashier, Information
                                System, e-commerce, or your other ideas? Our
                                team is ready to discuss with you to make your idea come true.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 py-0 wow d-flex align-items-stretch fadeInUp">
                        <div class="card card-body border-0 text-center shadow flex-fill pt-0">
                            <div class="svg-icon mx-auto mb-2">
                                <img class="img-fluid" src="theme/assets/img/icons/Android.png" alt="">
                            </div>
                            <h5 class="fg-gray">Mobile Development</h5>
                            <p class="fs-small text-justify">Do you want to create a mobile application ? Our team is
                                ready to help
                                you in create and develop application such as
                                simple applications, company profiles, e-commerce, marketplace, inventory, POS and other
                                ideas.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 py-0 wow d-flex align-items-stretch fadeInRight">
                        <div class="card card-body border-0 text-center shadow flex-fill pt-0">
                            <div class="svg-icon mx-auto mb-2">
                                <img class="img-fluid" src="theme/assets/img/icons/Desain.png" alt="">
                            </div>
                            <h5 class="fg-gray">Design</h5>
                            <p class="fs-small text-justify">Our team is ready to help you to create design such as
                                logo, handbooks,
                                UI / UX mobile, UI/UX website, branding company,
                                creative proposals, creative CV, brochures, posters, banners, etc.</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center ">
                    <a href="/portofolio" class="btn btn-warning">Read More</a>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>


<!-- Logos -->
<div class="page-section">
    <div class="container">
        <div class="text-center wow fadeIn">
            <h2 class="mb-4 text-warning">Supporting Professional</h2>
        </div>
        <div
            class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-5 justify-content-center align-items-center mt-5">
            @foreach ($supporting as $item)
            <div class="p-3 wow zoomIn">
                <div class="img-place client-img">
                    <img src="/images/supporting/{{ $item->gambar }}" alt="">
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>


@endsection
