@extends('layouts.admin.app')

@section('content')

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">New Portofolio</h1>
    </div>



    <!-- Content Row -->

    <div class="row">


        <div class="col-xl-8 col-lg-7">
            <div class="card shadow mb-4">
                <form action="/android/store" method="post" class="m-3" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Judul</label>
                        <input name="judul" class="form-control" type="text" placeholder="Judul">
                    </div>
                    <div class="form-group">
                        <label>Kategori</label>
                        <select name="kategori" class="form-control">
                            @foreach ($kategori as $category)
                            <option {{$category->id == $selected ? 'selected':'disabled'}} value="{{$category->id}}">
                                {{$category->nama}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Sub Kategori</label>
                        <select name="subkategori" class="form-control">
                            <option disabled selected>Choose One!</option>
                            @foreach ($subkategori as $sub)
                            <option value="{{$sub->id}}">
                                {{$sub->nama}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Foto Judul</label>
                        <input name="foto_judul" class="form-control" type="file">
                    </div>
                    <div class="form-group">
                        <label>Foto Isi</label>
                        <input name="foto_isi" class="form-control" type="file">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>

    </div>


</div>


@endsection
