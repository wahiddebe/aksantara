<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Android extends Model
{
    public function kategori()
    {
        return $this->belongsTo(Kategori::class);
    }
    public function subkategori()
    {
        return $this->belongsTo(Subkategori::class);
    }
}
