@extends('layouts.user.master')

@section('content')


@extends('layouts.user.master')

@section('content')

<main class="bg-light">

    <div class="page-hero-section bg-image hero-mini" style="background-image: url(theme/assets/img/hero_mini.svg);">
        <div class="hero-caption">
            <div class="container fg-white h-100">
                <div class="row justify-content-center align-items-center text-center h-100">
                    <div class="col-lg-6">
                        <h3 class="mb-4 fw-medium text-warning">Supporting Professional</h3>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-dark justify-content-center bg-transparent">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Supporting Professional</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-section">
        <div class="container">
            <div class="text-center wow fadeIn">
                <h2 class="mb-4 text-warning">Supporting Professional</h2>
            </div>
            <div
                class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-5 justify-content-center align-items-center mt-5">
                @foreach ($supporting as $item)
                <div class="p-3 wow zoomIn">
                    <div class="img-place client-img">
                        <img src="/images/supporting/{{ $item->gambar }}" alt="">
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

</main> <!-- .bg-light -->


@endsection

@endsection
