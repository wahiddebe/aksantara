<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        return view('admin.setting.index', [
            'settings' => Setting::latest()->get(),
        ]);
    }
    public function edit(Setting $setting)
    {
        return view('admin.setting.edit', [
            'settings' => $setting
        ]);
    }
    public function update(Request $request, Setting $setting)
    {
        $setting->no_hp = $request->no_hp;
        $setting->facebook = $request->facebook;
        $setting->twitter = $request->twitter;
        $setting->instagram = $request->instagram;
        $setting->behance = $request->behance;
        $setting->email = $request->email;
        $setting->update();
        return redirect()->to('setting');
    }
}
