@extends('layouts.user.master')

@section('content')
<div class="bg-light">

    <div class="page-hero-section bg-image hero-mini" style="background-image: url(theme/assets/img/hero_mini.svg);">
        <div class="hero-caption">
            <div class="container fg-white h-100">
                <div class="row justify-content-center align-items-center text-center h-100">
                    <div class="col-lg-6">
                        <h3 class="mb-4 fw-medium text-warning">Contact</h3>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-dark justify-content-center bg-transparent">
                                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Contact</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10 my-3 wow fadeInUp">
                    <div class="card-page">
                        <div class="row row-beam-md">
                            <div class="col-md-4 text-center py-3 py-md-2">
                                <i class="mai-location-outline h3"></i>
                                <p class="fg-primary fw-medium fs-vlarge">Location</p>
                                <p class="mb-0">Jl. Depoksari Raya No.29 Semarang, Jawa Tengah, Indonesia</p>
                            </div>
                            <div class="col-md-4 text-center py-3 py-md-2">
                                <i class="mai-call-outline h3"></i>
                                <p class="fg-primary fw-medium fs-vlarge">Contact</p>
                                <p class="mb-1">{{ $settings->no_hp }}</p>
                            </div>
                            <div class="col-md-4 text-center py-3 py-md-2">
                                <i class="mai-mail-open-outline h3"></i>
                                <p class="fg-primary fw-medium fs-vlarge">Email</p>
                                <p class="mb-1">{{ $settings->email }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-5 my-3 wow fadeInUp">
                    <div class="card-page">
                        <h3 class="fw-normal">Got anything to say ?</h3>
                        <p>Drop your message below</p>
                        <form action="/contact/store" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name" class="fw-medium fg-grey">Fullname</label>
                                <input type="text" class="form-control" name="nama" id="name">
                            </div>

                            <div class="form-group">
                                <label for="email" class="fw-medium fg-grey">Email</label>
                                <input type="text" class="form-control" name="email" id="email">
                            </div>

                            <div class="form-group">
                                <label for="phone" class="fw-medium fg-grey">Phone(optional)</label>
                                <input type="number" class="form-control" name="phone" id="phone">
                            </div>

                            <div class="form-group">
                                <label for="message" class="fw-medium fg-grey">Message</label>
                                <textarea rows="6" class="form-control" name="message" id="message"></textarea>
                            </div>


                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-primary">Send Message</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 col-lg-7 my-3 wow fadeInUp">
                    <div class="card-page">
                        <div class="maps-container">
                            <div id="myMap">
                                <iframe allowfullscreen="" frameborder="0"
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d990.0284673897679!2d110.46429382917752!3d-6.9958686685637845!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNsKwNTknNDUuMSJTIDExMMKwMjcnNTMuNCJF!5e0!3m2!1sid!2sid!4v1608902860285!5m2!1sid!2sid"
                                    width="100%" height="550"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
