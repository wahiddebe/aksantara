<?php

namespace App\Http\Controllers;

use App\Android;
use App\Kategori;
use App\Subkategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AndroidController extends Controller
{
    public function create()
    {
        return view("admin.android.create", [
            'selected' => 2,
            'kategori' => Kategori::get(),
            'subkategori'  => Subkategori::get(),
        ]);
    }
    public function edit(Android $android)
    {
        return view("admin.android.edit", [
            'android' => $android,
            'kategori' => Kategori::get(),
            'subkategori'  => Subkategori::get(),
        ]);
    }
    public function store(Request $request)
    {


        $request->validate([
            'foto_isi' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            'foto_judul' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
        ]);
        if ($request->hasFile('foto_judul')) {
            $foto_judul = $request->judul . '.' . $request->foto_judul->extension();
            $request->foto_judul->move(public_path('images/android/foto_judul'), $foto_judul);
        }
        if ($request->file('foto_judul') == null) {
            $foto_judul = null;
        }
        if ($request->hasFile('foto_isi')) {
            $foto_isi = $request->judul . '.' . $request->foto_isi->extension();
            $request->foto_isi->move(public_path('images/android/foto_isi'), $foto_isi);
        }
        if ($request->file('foto_isi') == null) {
            $foto_isi = null;
        }



        $android = new Android();
        $android->judul = $request->judul;
        $android->kategori_id = $request->kategori;
        $android->subkategori_id = $request->subkategori;
        $android->foto_judul = $foto_judul;
        $android->foto_isi = $foto_isi;
        $android->save();
        return redirect()->to('android');
    }
    public function update(Request $request, Android $android)
    {
        $request->validate([
            'foto_isi' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            'foto_judul' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
        ]);
        if ($request->hasFile('foto_judul')) {
            $foto_judul = $request->judul . '.' . $request->foto_judul->extension();
            $request->foto_judul->move(public_path('images/android/foto_judul'), $foto_judul);
        }
        if ($request->file('foto_judul') == null) {
            $foto_judul = $android->foto_judul;
        }
        if ($request->hasFile('foto_isi')) {
            $foto_isi = $request->judul . '.' . $request->foto_isi->extension();
            $request->foto_isi->move(public_path('images/android/foto_isi'), $foto_isi);
        }
        if ($request->file('foto_isi') == null) {
            $foto_isi = $android->foto_isi;
        }


        $android->judul = $request->judul;
        $android->kategori_id = $request->kategori;
        $android->subkategori_id = $request->subkategori;
        $android->foto_judul = $foto_judul;
        $android->foto_isi = $foto_isi;
        $android->update();
        return redirect()->to('android');
    }
    public function destroy(Android $android)
    {
        Storage::disk('aset')->delete('/images/android/foto_judul/' . $android->foto_judul);
        Storage::disk('aset')->delete('/images/android/foto_isi/' . $android->foto_isi);
        $android->delete();
        return redirect('android');
    }
}
