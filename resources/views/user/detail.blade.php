@extends('layouts.user.master')

@section('content')
<main>
    <div class="page-hero-section bg-image hero-mini"
        style="background-image: url({{ asset('theme/assets/img/hero_mini.svg') }});">
        <div class="hero-caption">
            <div class="container fg-white h-100">
                <div class="row justify-content-center align-items-center text-center h-100">
                    <div class="col-lg-6">
                        <h3 class="mb-4 fw-medium text-warning">{{ $items->judul }}
                        </h3>
                        <nav aria-label=" breadcrumb">
                            <ol class="breadcrumb breadcrumb-dark justify-content-center bg-transparent">
                                <li class="breadcrumb-item"><a href="/{{ $url.'portofolio' }}">{{ $title }}
                                        Portofolio</a></li>
                                <li class="breadcrumb-item active" aria-current="page">{{ $items->judul }}
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 py-3">
                    <article class="blog-single-entry">
                        <h1 class="post-title">{{ $items->judul }}</h1>
                        <div class="meta-item">
                            <div class="icon">
                                <span class="mai-pricetags"></span>
                            </div>
                            Category:
                            <a href="/subkategori/{{ $items->subkategori->id }}">{{ $items->subkategori->nama }}</a>
                        </div>
                        <div class="post-thumbnail">
                            <img src="{{ asset('images/'. $url.'/foto_isi/'. $items->foto_isi) }}" alt=""
                                class="img-fliud">
                        </div>
                    </article>

                </div>

                <!-- Sidebar -->
                <div class="col-lg-4 py-3">
                    <div class="widget-wrap">
                        <h3 class="widget-title">Category</h3>
                        <div class="tag-clouds">
                            @foreach ($subkategoris as $item)
                            <a href="/subkategori/{{ $item->id }}" class="tag-cloud-link">{{ $item->nama }}</a>
                            @endforeach
                        </div>
                    </div>
                </div> <!-- end sidebar -->

            </div> <!-- .row -->
        </div>
    </div>

</main>


@endsection
