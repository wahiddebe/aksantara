@extends('layouts.admin.app')

@section('content')

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Settings</h1>
    </div>

    <!-- Content Row -->

    <div class="row">


        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4 p-2">
                <table width="100%" class=" table table-striped table-bordered table-hover" id="table">
                    <thead>
                        <tr>
                            <th>No Hp</th>
                            <th>Facebook</th>
                            <th>Instagram</th>
                            <th>Twitter</th>
                            <th>Behance</th>
                            <th>Email</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($settings as $item)
                        <tr>
                            <td>{{ $item->no_hp }}</td>
                            <td>{{ $item->facebook }}</td>
                            <td>{{ $item->instagram }}</td>
                            <td>{{ $item->twitter }}</td>
                            <td>{{ $item->behance }}</td>
                            <td>{{ $item->email }}</td>
                            <td>
                                <div class="button-group">
                                    <a href="/setting/{{ $item->id }}/edit"><button
                                            class="btn-success btn">Edit</button></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>

    </div>


</div>


@endsection
