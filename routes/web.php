<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//  USER

Route::get('/about', 'UserController@about')->name('about');
Route::get('/', 'UserController@index')->name('welcome');
Route::get('/portofolio', 'UserController@portofolio')->name('portofolio');
Route::get('/support', 'UserController@support')->name('support');

Route::post('/contact/store', 'ContactController@store');
Route::get('/contact', 'UserController@contact')->name('contact');

Route::get('/webportofolio', 'UserController@webportofolio')->name('webportofolio');
Route::get('/androidportofolio', 'UserController@androidportofolio')->name('androidportofolio');
Route::get('/designportofolio', 'UserController@designportofolio')->name('designportofolio');

Route::get('/webportofolio/{web:id}', 'UserController@webdetail');
Route::get('/androidportofolio/{android:id}', 'UserController@androiddetail');
Route::get('/designportofolio/{design:id}', 'UserController@designdetail');

Route::get('/subkategori/{subkategori:id}', 'UserController@filter');

// ADMIN

Route::middleware('auth')->group(function () {
    //Admin
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/dashboard', 'AdminController@index')->name('dashboard');
    Route::get('/message', 'AdminController@message')->name('message');
    // Web
    Route::get('/web', 'AdminController@web')->name('web');
    Route::get('/web/create', 'WebController@create')->name('create_web');
    Route::post('/web/store', 'WebController@store');
    Route::get('/web/{web:id}/edit', 'WebController@edit');
    Route::patch('/web/{web:id}/edit', 'WebController@update');
    Route::delete('/web/{web:id}/delete', 'WebController@destroy');
    // Android
    Route::get('/android', 'AdminController@android')->name('android');
    Route::get('/android/create', 'AndroidController@create')->name('create_android');
    Route::post('/android/store', 'AndroidController@store');
    Route::get('/android/{android:id}/edit', 'AndroidController@edit');
    Route::patch('/android/{android:id}/edit', 'AndroidController@update');
    Route::delete('/android/{android:id}/delete', 'AndroidController@destroy');
    // Design
    Route::get('/design', 'AdminController@design')->name('design');
    Route::get('/design/create', 'DesignController@create')->name('create_design');
    Route::post('/design/store', 'DesignController@store');
    Route::get('/design/{design:id}/edit', 'DesignController@edit');
    Route::patch('/design/{design:id}/edit', 'DesignController@update');
    Route::delete('/design/{design:id}/delete', 'DesignController@destroy');
    // support
    Route::get('/supporting', 'AdminController@supporting')->name('supporting');
    Route::get('/supporting/create', 'SupportingController@create')->name('create_supporting');
    Route::post('/supporting/store', 'SupportingController@store');
    Route::get('/supporting/{supporting:id}/edit', 'SupportingController@edit');
    Route::patch('/supporting/{supporting:id}/edit', 'SupportingController@update');
    Route::delete('/supporting/{supporting:id}/delete', 'SupportingController@destroy');
    // setting
    Route::get('/setting', 'SettingController@index')->name('setting');
    Route::get('/setting/{setting:id}/edit', 'SettingController@edit');
    Route::patch('/setting/{setting:id}/edit', 'SettingController@update');
    // subkategori
    Route::get('/sub', 'SubkategoriController@index')->name('subkategori');
    Route::get('/sub/create', 'SubkategoriController@create')->name('create_subkategori');
    Route::post('/sub/store', 'SubkategoriController@store');
    Route::get('/sub/{subkategori:id}/edit', 'SubkategoriController@edit');
    Route::patch('/sub/{subkategori:id}/edit', 'SubkategoriController@update');
    Route::delete('/sub/{subkategori:id}/delete', 'SubkategoriController@destroy');
});
Auth::routes();
