<?php

namespace App\Http\Controllers;

use App\Design;
use App\Kategori;
use App\Subkategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DesignController extends Controller
{
    public function create()
    {
        return view("admin.design.create", [
            'selected' => 3,
            'kategori' => Kategori::get(),
            'subkategori'  => Subkategori::get(),
        ]);
    }
    public function edit(Design $design)
    {
        return view("admin.design.edit", [
            'design' => $design,
            'kategori' => Kategori::get(),
            'subkategori'  => Subkategori::get(),
        ]);
    }
    public function store(Request $request)
    {


        $request->validate([
            'foto_isi' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            'foto_judul' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
        ]);
        if ($request->hasFile('foto_judul')) {
            $foto_judul = $request->judul . '.' . $request->foto_judul->extension();
            $request->foto_judul->move(public_path('images/design/foto_judul'), $foto_judul);
        }
        if ($request->file('foto_judul') == null) {
            $foto_judul = null;
        }
        if ($request->hasFile('foto_isi')) {
            $foto_isi = $request->judul . '.' . $request->foto_isi->extension();
            $request->foto_isi->move(public_path('images/design/foto_isi'), $foto_isi);
        }
        if ($request->file('foto_isi') == null) {
            $foto_isi = null;
        }



        $design = new Design();
        $design->judul = $request->judul;
        $design->kategori_id = $request->kategori;
        $design->subkategori_id = $request->subkategori;
        $design->foto_judul = $foto_judul;
        $design->foto_isi = $foto_isi;
        $design->save();
        return redirect()->to('design');
    }
    public function update(Request $request, Design $design)
    {
        $request->validate([
            'foto_isi' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            'foto_judul' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
        ]);
        if ($request->hasFile('foto_judul')) {
            $foto_judul = $request->judul . '.' . $request->foto_judul->extension();
            $request->foto_judul->move(public_path('images/design/foto_judul'), $foto_judul);
        }
        if ($request->file('foto_judul') == null) {
            $foto_judul = $design->foto_judul;
        }
        if ($request->hasFile('foto_isi')) {
            $foto_isi = $request->judul . '.' . $request->foto_isi->extension();
            $request->foto_isi->move(public_path('images/design/foto_isi'), $foto_isi);
        }
        if ($request->file('foto_isi') == null) {
            $foto_isi = $design->foto_isi;
        }


        $design->judul = $request->judul;
        $design->kategori_id = $request->kategori;
        $design->subkategori_id = $request->subkategori;
        $design->foto_judul = $foto_judul;
        $design->foto_isi = $foto_isi;
        $design->update();
        return redirect()->to('design');
    }
    public function destroy(Design $design)
    {
        Storage::disk('aset')->delete('/images/design/foto_judul/' . $design->foto_judul);
        Storage::disk('aset')->delete('/images/design/foto_isi/' . $design->foto_isi);
        $design->delete();
        return redirect('design');
    }
}
