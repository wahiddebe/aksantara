<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="description" content="Aksantara Digital">


    <title>Aksantara Digital</title>
    <link href='https://fonts.googleapis.com/css?family=Nunito Sans' rel='stylesheet'>
    <link rel="shortcut icon" href="{{ asset('theme/assets/favicon-light.png') }}" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset('theme/assets/css/maicons.css') }}">

    <link rel="stylesheet" href="{{ asset('theme/assets/vendor/animate/animate.css') }}">

    <link rel="stylesheet" href="{{ asset('theme/assets/vendor/owl-carousel/css/owl.carousel.min.css') }}">

    <link rel="stylesheet" href="{{ asset('theme/assets/css/bootstrap.css') }}">

    <link rel="stylesheet" href="{{ asset('theme/assets/css/mobster.css') }}">
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark navbar-floating">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="{{ asset('theme/assets/favicon-light.png') }}" alt="" width="60">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler"
                aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarToggler">
                <ul class="navbar-nav ml-auto mt-3 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="/about">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/portofolio">Portofolio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/support">Supporting Professional</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/contact">Contact Us</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
