@extends('layouts.user.master')

@section('content')
<main>
    <div class="page-hero-section bg-image hero-mini"
        style="background-image: url({{ asset('theme/assets/img/hero_mini.svg') }});">
        <div class="hero-caption">
            <div class="container fg-white h-100">
                <div class="row justify-content-center align-items-center text-center h-100">
                    <div class="col-lg-6">
                        <h3 class="mb-4 fw-medium text-warning">
                            {{ $subs->nama }}</h3>
                        <nav aria-label=" breadcrumb">
                            <ol class="breadcrumb breadcrumb-dark justify-content-center bg-transparent">
                                <li class="breadcrumb-item"><a href="/portofolio">Portofolio</a></li>
                                <li class="breadcrumb-item active" aria-current="page">{{ $subs->nama }}</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 py-3">
                    @foreach ($webs as $item)
                    <article class="blog-entry">
                        <div class="entry-header">
                            <div class="post-thumbnail">
                                <img src="{{ asset('images/web/foto_judul/'. $item->foto_judul) }}" alt="">
                            </div>
                        </div>
                        <div class="post-title">{{$item->judul}}</div>
                        <div class="entry-meta mb-2">
                            <div class="meta-item">
                                <div class="icon">
                                    <span class="mai-pricetags"></span>
                                </div>
                                Category:
                                <a href="/subkategori/{{ $item->subkategori->id }}">{{ $item->subkategori->nama }}</a>,
                                <a href="/webportofolio">{{ $item->kategori->nama }}</a>
                            </div>
                        </div>
                        <a href="/webportofolio/{{ $item->id }}" class="btn btn-warning">Read More</a>
                    </article>
                    @endforeach
                    @foreach ($androids as $item)
                    <article class="blog-entry">
                        <div class="entry-header">
                            <div class="post-thumbnail">
                                <img src="{{  asset('images/android/foto_judul/'. $item->foto_judul)  }}" alt="">
                            </div>
                        </div>
                        <div class="post-title">{{$item->judul}}</div>
                        <div class="entry-meta mb-2">
                            <div class="meta-item">
                                <div class="icon">
                                    <span class="mai-pricetags"></span>
                                </div>
                                Category:
                                <a href="/subkategori/{{ $item->subkategori->id }}">{{ $item->subkategori->nama }}</a>,
                                <a href="/androidportofolio">{{ $item->kategori->nama }}</a>
                            </div>
                        </div>
                        <a href="/androidportofolio/{{ $item->id }}" class="btn btn-warning">Read More</a>
                    </article>
                    @endforeach
                    @foreach ($designs as $item)
                    <article class="blog-entry">
                        <div class="entry-header">
                            <div class="post-thumbnail">
                                <img src="{{  asset('images/design/foto_judul/'. $item->foto_judul)  }}" alt="">
                            </div>
                        </div>
                        <div class="post-title">{{$item->judul}}</div>
                        <div class="entry-meta mb-2">
                            <div class="meta-item">
                                <div class="icon">
                                    <span class="mai-pricetags"></span>
                                </div>
                                Category:
                                <a href="/subkategori/{{ $item->subkategori->id }}">{{ $item->subkategori->nama }}</a>,
                                <a href="/designportofolio">{{ $item->kategori->nama }}</a>
                            </div>
                        </div>
                        <a href="/designportofolio/{{ $item->id }}" class="btn btn-warning">Read More</a>
                    </article>
                    @endforeach


                </div>
                <!-- Sidebar -->
                <div class="col-lg-4 py-3">
                    <div class="widget-wrap">
                        <h3 class="widget-title">Category</h3>
                        <div class="tag-clouds">
                            @foreach ($subkategoris as $item)
                            <a href="/subkategori/{{ $item->id }}" class="tag-cloud-link">{{ $item->nama }}</a>
                            @endforeach
                        </div>
                    </div>
                </div> <!-- end sidebar -->
            </div>
        </div>
    </div>

</main>


@endsection
