<?php

namespace App\Http\Controllers;

use App\Supporting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SupportingController extends Controller
{
    public function create()
    {
        return view("admin.support.create");
    }
    public function edit(Supporting $supporting)
    {
        return view("admin.support.edit", [
            'supporting' => $supporting,
        ]);
    }
    public function store(Request $request)
    {


        $request->validate([
            'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
        ]);
        if ($request->hasFile('gambar')) {
            $gambar = $request->nama . '.' . $request->gambar->extension();
            $request->gambar->move(public_path('images/supporting'), $gambar);
        }
        if ($request->file('gambar') == null) {
            $gambar = null;
        }



        $supporting = new Supporting();
        $supporting->nama = $request->nama;
        $supporting->gambar = $gambar;
        $supporting->save();
        return redirect()->to('supporting');
    }
    public function update(Request $request, Supporting $supporting)
    {
        $request->validate([
            'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
        ]);
        if ($request->hasFile('gambar')) {
            $gambar = $request->nama . '.' . $request->gambar->extension();
            $request->gambar->move(public_path('images/supporting'), $gambar);
        }
        if ($request->file('gambar') == null) {
            $gambar = $supporting->gambar;
        }


        $supporting->nama = $request->nama;
        $supporting->gambar = $gambar;
        $supporting->update();
        return redirect()->to('supporting');
    }
    public function destroy(Supporting $supporting)
    {
        Storage::disk('aset')->delete('/images/supporting/' . $supporting->gambar);
        $supporting->delete();
        return redirect('supporting');
    }
}
