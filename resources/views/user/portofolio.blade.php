@extends('layouts.user.master')

@section('content')

<main class="bg-light">

    <div class="page-hero-section bg-image hero-mini" style="background-image: url(theme/assets/img/hero_mini.svg);">
        <div class="hero-caption">
            <div class="container fg-white h-100">
                <div class="row justify-content-center align-items-center text-center h-100">
                    <div class="col-lg-6">
                        <h3 class="mb-4 fw-medium text-warning">Portofolio</h3>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-dark justify-content-center bg-transparent">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Portofolio</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-section no-scroll">
        <div class="container">
            <h2 class="text-center wow fadeIn mb-4 text-warning">Portofolio</h2>

            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="row justify-content-center mb-4 ">
                        <div class="col-md-6 col-lg-4 py-0 wow d-flex align-items-stretch fadeInLeft">
                            <a href="/webportofolio" class="link-warning">
                                <div class="card card-body border-0 text-center shadow flex-fill pt-0">
                                    <div class="svg-icon mx-auto mb-2">
                                        <img class="img-fluid" src="theme/assets/img/icons/Website.png" alt="">
                                    </div>
                                    <h5 class="fg-gray">Web Develpment</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 py-0 wow d-flex align-items-stretch fadeInUp">
                            <a href="/androidportofolio" class="link-warning">
                                <div class="card card-body border-0 text-center shadow flex-fill pt-0">
                                    <div class="svg-icon mx-auto mb-2">
                                        <img class="img-fluid" src="theme/assets/img/icons/Android.png" alt="">
                                    </div>
                                    <h5 class="fg-gray">Mobile Development</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 py-0 wow d-flex align-items-stretch fadeInRight">
                            <a class="link-warning" href="/designportofolio">
                                <div class="card card-body border-0 text-center shadow flex-fill pt-0">
                                    <div class="svg-icon mx-auto mb-2">
                                        <img class="img-fluid" src="theme/assets/img/icons/Desain.png" alt="">
                                    </div>
                                    <h5 class="fg-gray">Design</h5>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main> <!-- .bg-light -->


@endsection
