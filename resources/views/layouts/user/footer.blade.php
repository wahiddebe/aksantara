<div class="page-footer-section bg-dark fg-white" id="footer">
    <div class="container">
        <div class="row mb-5 py-3">
            <div class="col-md-6 col-lg-6 py-3">
                <h5 class="mb-3 text-warning">Contact Us</h5>
                <ul class="menu-link">
                    <li><a target="_blank" href="https://goo.gl/maps/VMhxYtkcX8CEM8n57" class="">Jl. Depoksari Raya
                            No.29
                            Semarang, Jawa Tengah, Indonesia</a></li>
                    <li><a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&to={{ $settings->email }}" class=""
                            target="_blank">{{ $settings->email }}</a></li>
                    <li><a class="">{{ $settings->no_hp }}</a></li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-6 py-3">
                <h5 class="mb-3 text-warning">Follow Us</h5>
                <p>Get some news or update information from us</p>

                <!-- Social Media Button -->
                <div class="mt-4">
                    <a href="{{ $settings->facebook }}" class="btn btn-fab btn-warning fg-white"><span
                            class="mai-logo-facebook"></span></a>
                    <a href="{{ $settings->twitter }}" class="btn btn-fab btn-warning fg-white"><span
                            class="mai-logo-twitter"></span></a>
                    <a href="{{ $settings->instagram }}" class="btn btn-fab btn-warning fg-white"><span
                            class="mai-logo-instagram"></span></a>
                    <a href="{{ $settings->behance }}" class="btn btn-fab btn-warning fg-white"><span
                            class="mai-logo-behance"></span></a>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="container ">
        <div class="rows">
            <div class="col-12 col-md-12 py-2">
                <img class="" src="theme/assets/favicon-light.png" alt="" width="40">
            </div>
        </div>
    </div>
</div>

<script src="theme/assets/js/jquery-3.5.1.min.js"></script>

<script src="theme/assets/js/bootstrap.bundle.min.js"></script>

<script src="theme/assets/vendor/owl-carousel/js/owl.carousel.min.js"></script>

<script src="theme/assets/vendor/wow/wow.min.js"></script>

<script src="theme/assets/js/mobster.js"></script>

</body>

</html>
