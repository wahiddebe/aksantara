<?php

namespace App\Http\Controllers;

use App\Android;
use App\Contact;
use App\Design;
use App\Setting;
use App\Supporting;
use App\Web;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.dashboard', [
            'web' => DB::table('webs')->count(),
            'android' => DB::table('androids')->count(),
            'design' => DB::table('designs')->count(),
            'message' => DB::table('contacts')->count(),
        ]);
    }
    public function web()
    {
        return view('admin.web.web', [
            'webs' => Web::latest()->get(),
        ]);
    }
    public function android()
    {
        return view('admin.android.android', [
            'androids' => Android::latest()->get(),
        ]);
    }
    public function design()
    {
        return view('admin.design.design', [
            'designs' => Design::latest()->get(),
        ]);
    }
    public function supporting()
    {
        return view('admin.support.support', [
            'supportings' => Supporting::latest()->get(),
        ]);
    }
    public function message()
    {
        return view('admin.message', [
            'contacts' => Contact::latest()->get(),
        ]);
    }
}
