@extends('layouts.admin.app')

@section('content')

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Settings</h1>
    </div>



    <!-- Content Row -->

    <div class="row">


        <div class="col-xl-8 col-lg-7">
            <div class="card shadow mb-4">
                <form method="POST" action="/setting/{{ $settings->id }}/edit" class="m-3">
                    @csrf
                    @method('patch')
                    <div class="form-group">
                        <label>No Hp</label>
                        <input name="no_hp" value="{{ $settings->no_hp }}" class="form-control" type="text"
                            placeholder="No Hp">
                    </div>
                    <div class="form-group">
                        <label>Facebook</label>
                        <input name="facebook" value="{{ $settings->facebook }}" class="form-control" type="text"
                            placeholder="Facebook">
                    </div>
                    <div class="form-group">
                        <label>Instagram</label>
                        <input name="instagram" value="{{ $settings->instagram }}" class="form-control" type="text"
                            placeholder="Instagram">
                    </div>
                    <div class="form-group">
                        <label>Twitter</label>
                        <input name="twitter" value="{{ $settings->twitter }}" class="form-control" type="text"
                            placeholder="Twitter">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input name="email" value="{{ $settings->email }}" class="form-control" type="text"
                            placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label>Behance</label>
                        <input name="behance" value="{{ $settings->behance }}" class="form-control" type="text"
                            placeholder="Behance">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>

    </div>


</div>


@endsection
